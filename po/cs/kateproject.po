# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2012.
# SPDX-FileCopyrightText: 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2023 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2023-11-02 13:59+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"

#: branchcheckoutdialog.cpp:29
#, kde-format
msgid "Select branch to checkout. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:36
#, kde-format
msgid "Create New Branch"
msgstr "Vytvořit novou větev"

#: branchcheckoutdialog.cpp:38
#, kde-format
msgid "Create New Branch From..."
msgstr "Vytvořit novou větev z..."

#: branchcheckoutdialog.cpp:53
#, kde-format
msgid "Branch %1 checked out"
msgstr ""

#: branchcheckoutdialog.cpp:56
#, kde-format
msgid "Failed to checkout to branch %1, Error: %2"
msgstr ""

#: branchcheckoutdialog.cpp:83 branchcheckoutdialog.cpp:95
#, kde-format
msgid "Enter new branch name. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:100
#, kde-format
msgid "Select branch to checkout from. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:121
#, kde-format
msgid "Checked out to new branch: %1"
msgstr ""

#: branchcheckoutdialog.cpp:123
#, kde-format
msgid "Failed to create new branch. Error \"%1\""
msgstr ""

#: branchdeletedialog.cpp:126
#, kde-format
msgid "Branch"
msgstr "Větev"

#: branchdeletedialog.cpp:126
#, kde-format
msgid "Last Commit"
msgstr "Poslední commit"

#: branchdeletedialog.cpp:139 kateprojecttreeviewcontextmenu.cpp:78
#, kde-format
msgid "Delete"
msgstr "Smazat"

#: branchdeletedialog.cpp:144
#, kde-format
msgid "Are you sure you want to delete the selected branch?"
msgid_plural "Are you sure you want to delete the selected branches?"
msgstr[0] "Opravdu si přejete odstranit vybranou větev?"
msgstr[1] "Opravdu si přejete odstranit vybrané větve?"
msgstr[2] "Opravdu si přejete odstranit vybrané větve?"

#: branchesdialog.cpp:100
#, kde-format
msgid "Select Branch..."
msgstr ""

#: branchesdialog.cpp:126 gitwidget.cpp:536 kateprojectpluginview.cpp:67
#, kde-format
msgid "Git"
msgstr "Git"

#: comparebranchesview.cpp:145
#, kde-format
msgid "Back"
msgstr "Zpět"

#: currentgitbranchbutton.cpp:124
#, kde-format
msgctxt "Tooltip text, describing that '%1' commit is checked out"
msgid "HEAD at commit %1"
msgstr ""

#: currentgitbranchbutton.cpp:126
#, kde-format
msgctxt "Tooltip text, describing that '%1' tag is checked out"
msgid "HEAD is at this tag %1"
msgstr ""

#: currentgitbranchbutton.cpp:128
#, kde-format
msgctxt "Tooltip text, describing that '%1' branch is checked out"
msgid "Active branch: %1"
msgstr "Aktivní větev: %1"

#: gitcommitdialog.cpp:69 gitcommitdialog.cpp:116
#, kde-format
msgid "Commit Changes"
msgstr ""

#: gitcommitdialog.cpp:73 gitcommitdialog.cpp:115 gitwidget.cpp:262
#, kde-format
msgid "Commit"
msgstr "Commit"

#: gitcommitdialog.cpp:74
#, kde-format
msgid "Cancel"
msgstr "Zrušit"

#: gitcommitdialog.cpp:76
#, kde-format
msgid "Write commit message..."
msgstr ""

#: gitcommitdialog.cpp:83
#, kde-format
msgid "Extended commit description..."
msgstr ""

#: gitcommitdialog.cpp:107
#, kde-format
msgid "Sign off"
msgstr "Podepsat"

#: gitcommitdialog.cpp:111 gitcommitdialog.cpp:120
#, kde-format
msgid "Amend"
msgstr ""

#: gitcommitdialog.cpp:112 gitwidget.cpp:1011
#, kde-format
msgid "Amend Last Commit"
msgstr "Doplnit poslední commit"

#: gitcommitdialog.cpp:119
#, kde-format
msgid "Amending Commit"
msgstr ""

#: gitcommitdialog.cpp:203
#, kde-format
msgctxt "Number of characters"
msgid "%1 / 52"
msgstr "%1 / 52"

#: gitcommitdialog.cpp:207
#, kde-format
msgctxt "Number of characters"
msgid "<span style=\"color:%1;\">%2</span> / 52"
msgstr "<span style=\"color:%1;\">%2</span> / 52"

#: gitstatusmodel.cpp:85
#, kde-format
msgid "Staged"
msgstr "Staged"

#: gitstatusmodel.cpp:87
#, kde-format
msgid "Untracked"
msgstr "Untracked"

#: gitstatusmodel.cpp:89
#, kde-format
msgid "Conflict"
msgstr "Konflikt"

#: gitstatusmodel.cpp:91
#, kde-format
msgid "Modified"
msgstr "Změněno"

#: gitwidget.cpp:270
#, kde-format
msgid "Git Push"
msgstr "Git Push"

#: gitwidget.cpp:283
#, kde-format
msgid "Git Pull"
msgstr "Git Pull"

#: gitwidget.cpp:296
#, kde-format
msgid "Cancel Operation"
msgstr "Zrušit operaci"

#: gitwidget.cpp:304
#, kde-format
msgid " canceled."
msgstr "zrušeno."

#: gitwidget.cpp:325 kateprojectview.cpp:59
#, kde-format
msgid "Filter..."
msgstr "Filtr..."

#: gitwidget.cpp:415
#, kde-format
msgid "Failed to find .git directory for '%1', things may not work correctly"
msgstr ""

#: gitwidget.cpp:620
#, kde-format
msgid " error: %1"
msgstr " chyba: %1"

#: gitwidget.cpp:626
#, kde-format
msgid "\"%1\" executed successfully: %2"
msgstr ""

#: gitwidget.cpp:646
#, kde-format
msgid "Failed to stage file. Error:"
msgstr ""

#: gitwidget.cpp:659
#, kde-format
msgid "Failed to unstage file. Error:"
msgstr ""

#: gitwidget.cpp:670
#, kde-format
msgid "Failed to discard changes. Error:"
msgstr ""

#: gitwidget.cpp:681
#, kde-format
msgid "Failed to remove. Error:"
msgstr ""

#: gitwidget.cpp:697
#, kde-format
msgid "Failed to open file at HEAD: %1"
msgstr ""

#: gitwidget.cpp:729
#, kde-format
msgid "Failed to get Diff of file: %1"
msgstr ""

#: gitwidget.cpp:796
#, kde-format
msgid "Failed to commit: %1"
msgstr ""

#: gitwidget.cpp:800
#, kde-format
msgid "Changes committed successfully."
msgstr ""

#: gitwidget.cpp:810
#, kde-format
msgid "Nothing to commit. Please stage your changes first."
msgstr ""

#: gitwidget.cpp:823
#, kde-format
msgid "Commit message cannot be empty."
msgstr ""

#: gitwidget.cpp:943
#, kde-format
msgid "No diff for %1...%2"
msgstr ""

#: gitwidget.cpp:949
#, kde-format
msgid "Failed to compare %1...%2"
msgstr ""

#: gitwidget.cpp:964
#, kde-format
msgid "Failed to get numstat when diffing %1...%2"
msgstr ""

#: gitwidget.cpp:1003
#, kde-format
msgid "Refresh"
msgstr "Obnovit"

#: gitwidget.cpp:1019
#, kde-format
msgid "Checkout Branch"
msgstr ""

#: gitwidget.cpp:1031
#, kde-format
msgid "Delete Branch"
msgstr "Smazat větev"

#: gitwidget.cpp:1043
#, kde-format
msgid "Compare Branch with..."
msgstr "Porovnat větev s..."

#: gitwidget.cpp:1048
#, kde-format
msgid "Show Commit"
msgstr "Zobrazit Commit"

#: gitwidget.cpp:1048
#, kde-format
msgid "Commit hash"
msgstr ""

#: gitwidget.cpp:1055
#, kde-format
msgid "Open Commit..."
msgstr "Otevřít Commit..."

#: gitwidget.cpp:1058 gitwidget.cpp:1097
#, kde-format
msgid "Stash"
msgstr "Uschovat"

#: gitwidget.cpp:1068
#, kde-format
msgid "Diff - stash"
msgstr ""

#: gitwidget.cpp:1101
#, kde-format
msgid "Pop Last Stash"
msgstr ""

#: gitwidget.cpp:1105
#, kde-format
msgid "Pop Stash"
msgstr ""

#: gitwidget.cpp:1109
#, kde-format
msgid "Apply Last Stash"
msgstr ""

#: gitwidget.cpp:1111
#, kde-format
msgid "Stash (Keep Staged)"
msgstr ""

#: gitwidget.cpp:1115
#, kde-format
msgid "Stash (Include Untracked)"
msgstr ""

#: gitwidget.cpp:1119
#, kde-format
msgid "Apply Stash"
msgstr ""

#: gitwidget.cpp:1120
#, kde-format
msgid "Drop Stash"
msgstr ""

#: gitwidget.cpp:1121
#, kde-format
msgid "Show Stash Content"
msgstr ""

#: gitwidget.cpp:1157
#, kde-format
msgid "Stage All"
msgstr ""

#: gitwidget.cpp:1159
#, kde-format
msgid "Remove All"
msgstr "Odstranit vše"

#: gitwidget.cpp:1159
#, kde-format
msgid "Discard All"
msgstr "Zahodit vše"

#: gitwidget.cpp:1162
#, kde-format
msgid "Open .gitignore"
msgstr ""

#: gitwidget.cpp:1163 gitwidget.cpp:1210 gitwidget.cpp:1252
#: kateprojectconfigpage.cpp:88 kateprojectconfigpage.cpp:100
#, kde-format
msgid "Show Diff"
msgstr "Zobrazit změny"

#: gitwidget.cpp:1180
#, kde-format
msgid "Are you sure you want to remove these files?"
msgstr "Jste si jistí, že si přejete odstranit tyto soubory?"

#: gitwidget.cpp:1185
#, kde-format
msgid "Are you sure you want to discard all changes?"
msgstr "Opravdu si přejete zahodit všechny změny?"

#: gitwidget.cpp:1209
#, kde-format
msgid "Open File"
msgstr "Otevřít soubor"

#: gitwidget.cpp:1211
#, kde-format
msgid "Show in External Git Diff Tool"
msgstr ""

#: gitwidget.cpp:1212
#, kde-format
msgid "Open at HEAD"
msgstr ""

#: gitwidget.cpp:1213
#, kde-format
msgid "Unstage File"
msgstr ""

#: gitwidget.cpp:1213
#, kde-format
msgid "Stage File"
msgstr ""

#: gitwidget.cpp:1214
#, kde-format
msgid "Remove"
msgstr "Odstranit"

#: gitwidget.cpp:1214
#, kde-format
msgid "Discard"
msgstr "Zahodit"

#: gitwidget.cpp:1231
#, kde-format
msgid "Are you sure you want to discard the changes in this file?"
msgstr ""

#: gitwidget.cpp:1240
#, kde-format
msgid "Are you sure you want to remove this file?"
msgstr "Opravdu si přejete odstranit tento soubor?"

#: gitwidget.cpp:1251
#, kde-format
msgid "Unstage All"
msgstr ""

#: gitwidget.cpp:1318
#, kde-format
msgid "Unstage Selected Files"
msgstr ""

#: gitwidget.cpp:1318
#, kde-format
msgid "Stage Selected Files"
msgstr ""

#: gitwidget.cpp:1319
#, kde-format
msgid "Discard Selected Files"
msgstr ""

#: gitwidget.cpp:1323
#, kde-format
msgid "Remove Selected Files"
msgstr "Odstranit vybrané soubory"

#: gitwidget.cpp:1336
#, kde-format
msgid "Are you sure you want to discard the changes?"
msgstr ""

#: gitwidget.cpp:1341
#, kde-format
msgid "Are you sure you want to remove these untracked changes?"
msgstr ""

#: kateproject.cpp:218
#, kde-format
msgid "Malformed JSON file '%1': %2"
msgstr ""

#: kateproject.cpp:518
#, kde-format
msgid "<untracked>"
msgstr "<nesledováno>"

#: kateprojectcompletion.cpp:54
#, kde-format
msgid "Project Completion"
msgstr "Doplňování projektu"

#: kateprojectconfigpage.cpp:24
#, kde-format
msgctxt "Groupbox title"
msgid "Autoload Repositories"
msgstr "Automaticky načítat repozitáře"

#: kateprojectconfigpage.cpp:26
#, kde-format
msgid ""
"Project plugin is able to autoload repository working copies when there is "
"no .kateproject file defined yet."
msgstr ""
"Modul projektu je schopen automaticky načíst repozitář pracovních kopií "
"pokud ještě není určen soubor .kateproject."

#: kateprojectconfigpage.cpp:29
#, kde-format
msgid "&Git"
msgstr "&Git"

#: kateprojectconfigpage.cpp:32
#, kde-format
msgid "&Subversion"
msgstr "&Subversion"

#: kateprojectconfigpage.cpp:34
#, kde-format
msgid "&Mercurial"
msgstr "&Mercurial"

#: kateprojectconfigpage.cpp:36
#, kde-format
msgid "&Fossil"
msgstr ""

#: kateprojectconfigpage.cpp:45
#, kde-format
msgctxt "Groupbox title"
msgid "Session Behavior"
msgstr "Chování sezení"

#: kateprojectconfigpage.cpp:46
#, kde-format
msgid "Session settings for projects"
msgstr "Nastavení sezení pro projekty"

#: kateprojectconfigpage.cpp:47
#, kde-format
msgid "Restore Open Projects"
msgstr "Obnovit otevřené projekty"

#: kateprojectconfigpage.cpp:54
#, kde-format
msgctxt "Groupbox title"
msgid "Project Index"
msgstr "Index projektu"

#: kateprojectconfigpage.cpp:55
#, kde-format
msgid "Project ctags index settings"
msgstr ""

#: kateprojectconfigpage.cpp:56 kateprojectinfoviewindex.cpp:209
#, kde-format
msgid "Enable indexing"
msgstr "Povolit indexování"

#: kateprojectconfigpage.cpp:59
#, kde-format
msgid "Directory for index files"
msgstr "Adresář pro soubory indexu"

#: kateprojectconfigpage.cpp:63
#, kde-format
msgid ""
"The system temporary directory is used if not specified, which may overflow "
"for very large repositories"
msgstr ""

#: kateprojectconfigpage.cpp:70
#, kde-format
msgctxt "Groupbox title"
msgid "Cross-Project Functionality"
msgstr "Funkčnost napříč projekty"

#: kateprojectconfigpage.cpp:71
#, kde-format
msgid ""
"Project plugin is able to perform some operations across multiple projects"
msgstr ""

#: kateprojectconfigpage.cpp:72
#, kde-format
msgid "Cross-Project Completion"
msgstr ""

#: kateprojectconfigpage.cpp:74
#, kde-format
msgid "Cross-Project Goto Symbol"
msgstr ""

#: kateprojectconfigpage.cpp:82
#, kde-format
msgctxt "Groupbox title"
msgid "Git"
msgstr "Git"

#: kateprojectconfigpage.cpp:85
#, kde-format
msgid "Single click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:87 kateprojectconfigpage.cpp:99
#, kde-format
msgid "No Action"
msgstr "Žádná činnost"

#: kateprojectconfigpage.cpp:89 kateprojectconfigpage.cpp:101
#, kde-format
msgid "Open file"
msgstr "Otevřít soubor"

#: kateprojectconfigpage.cpp:90 kateprojectconfigpage.cpp:102
#, kde-format
msgid "Stage / Unstage"
msgstr ""

#: kateprojectconfigpage.cpp:97
#, kde-format
msgid "Double click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:133 kateprojectpluginview.cpp:66
#, kde-format
msgid "Projects"
msgstr "Projekty"

#: kateprojectconfigpage.cpp:138
#, kde-format
msgctxt "Groupbox title"
msgid "Projects Properties"
msgstr "Vlastnosti projektu"

#: kateprojectinfoview.cpp:36
#, kde-format
msgid "Terminal (.kateproject)"
msgstr "Terminál (.kateproject)"

#: kateprojectinfoview.cpp:44
#, kde-format
msgid "Terminal (Base)"
msgstr "Terminál (Základ)"

#: kateprojectinfoview.cpp:51
#, kde-format
msgid "Code Index"
msgstr "Index kódu"

#: kateprojectinfoview.cpp:56
#, kde-format
msgid "Code Analysis"
msgstr "Analýza kódu"

#: kateprojectinfoview.cpp:61
#, kde-format
msgid "Notes"
msgstr "Poznámky"

#: kateprojectinfoviewcodeanalysis.cpp:35
#, kde-format
msgid "Start Analysis..."
msgstr "Spustit analýzu..."

#: kateprojectinfoviewcodeanalysis.cpp:43
#, kde-format
msgctxt "'%1' refers to project name, e.g,. Code Analysis - MyProject"
msgid "Code Analysis - %1"
msgstr "Analýza kódu - %1"

#: kateprojectinfoviewcodeanalysis.cpp:100
#, kde-format
msgid ""
"'%1' is not installed on your system, %2.<br/><br/>%3. The tool will be run "
"on all project files which match this list of file extensions:<br/><b>%4</b>"
msgstr ""

#: kateprojectinfoviewcodeanalysis.cpp:110
#, kde-format
msgid ""
"Using %1 installed at: %2.<br/><br/>%3. The tool will be run on all project "
"files which match this list of file extensions:<br/><b>%4</b>"
msgstr ""

#: kateprojectinfoviewcodeanalysis.cpp:152
#: kateprojectinfoviewcodeanalysis.cpp:212
#: kateprojectinfoviewcodeanalysis.cpp:216
#, kde-format
msgid "CodeAnalysis"
msgstr "Analýza kódu"

#: kateprojectinfoviewcodeanalysis.cpp:207
#, kde-format
msgctxt ""
"Message to the user that analysis finished. %1 is the name of the program "
"that did the analysis, %2 is a number. e.g., [clang-tidy]Analysis on 5 files "
"finished"
msgid "[%1]Analysis on %2 file finished."
msgid_plural "[%1]Analysis on %2 files finished."
msgstr[0] "[%1] Analýza %2 souboru byla dokončena."
msgstr[1] "[%1] Analýza %2 souborů byla dokončena."
msgstr[2] "[%1] Analýza %2 souborů byla dokončena."

#: kateprojectinfoviewcodeanalysis.cpp:215
#, kde-format
msgid "Analysis failed with exit code %1, Error: %2"
msgstr "Analýza selhala s kódem %1. Chyba: %2."

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Name"
msgstr "Název"

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Kind"
msgstr "Typ"

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "File"
msgstr "Soubor"

#: kateprojectinfoviewindex.cpp:35
#, kde-format
msgid "Line"
msgstr "Řádek"

#: kateprojectinfoviewindex.cpp:36
#, kde-format
msgid "Search"
msgstr "Hledat"

#: kateprojectinfoviewindex.cpp:198
#, kde-format
msgid "The index could not be created. Please install 'ctags'."
msgstr "Nelze vytvořit index. Prosím, nainstalujte 'ctags'."

#: kateprojectinfoviewindex.cpp:208
#, kde-format
msgid "Indexing is not enabled"
msgstr "Indexování není povoleno"

#: kateprojectitem.cpp:166
#, kde-format
msgid "Error"
msgstr "Chyba"

#: kateprojectitem.cpp:166
#, kde-format
msgid "File name already exists"
msgstr "Název souboru již existuje."

#: kateprojectplugin.cpp:217
#, kde-format
msgid "Confirm project closing: %1"
msgstr ""

#: kateprojectplugin.cpp:218
#, kde-format
msgid "Do you want to close the project %1 and the related %2 open documents?"
msgstr ""

#: kateprojectplugin.cpp:557
#, kde-format
msgid "Full path to current project excluding the file name."
msgstr ""

#: kateprojectplugin.cpp:574
#, kde-format
msgid ""
"Full path to current project excluding the file name, with native path "
"separator (backslash on Windows)."
msgstr ""

#: kateprojectplugin.cpp:691 kateprojectpluginview.cpp:72
#: kateprojectpluginview.cpp:233 kateprojectviewtree.cpp:136
#: kateprojectviewtree.cpp:157
#, kde-format
msgid "Project"
msgstr "Projekt"

#: kateprojectpluginview.cpp:56
#, kde-format
msgid "Project Manager"
msgstr "Správce projektu"

#: kateprojectpluginview.cpp:78
#, kde-format
msgid "Open projects list"
msgstr "Otevřít seznam projektů"

#: kateprojectpluginview.cpp:83
#, kde-format
msgid "Reload project"
msgstr "Znovu načíst projekt"

#: kateprojectpluginview.cpp:86
#, kde-format
msgid "Close project"
msgstr "Zavřít projekt"

#: kateprojectpluginview.cpp:106
#, kde-format
msgid "Refresh git status"
msgstr "Obnovit stav gitu"

#: kateprojectpluginview.cpp:184
#, kde-format
msgid "Open Folder..."
msgstr "Otevřít složku..."

#: kateprojectpluginview.cpp:189
#, kde-format
msgid "Project TODOs"
msgstr "Úlohy projektu"

#: kateprojectpluginview.cpp:193
#, kde-format
msgid "Activate Previous Project"
msgstr "Aktivovat předchozí projekt"

#: kateprojectpluginview.cpp:198
#, kde-format
msgid "Activate Next Project"
msgstr "Aktivovat další projekt"

#: kateprojectpluginview.cpp:203
#, kde-format
msgid "Lookup"
msgstr "Vyhledat"

#: kateprojectpluginview.cpp:207
#, kde-format
msgid "Close Project"
msgstr "Zavřít projekt"

#: kateprojectpluginview.cpp:211
#, kde-format
msgid "Close All Projects"
msgstr "Zavřít všechny projekty"

#: kateprojectpluginview.cpp:216
#, kde-format
msgid "Close Orphaned Projects"
msgstr "Zavřít osiřelé projekty"

#: kateprojectpluginview.cpp:220
#, kde-format
msgid "Reload Project"
msgstr "Znovu načíst projekt"

#: kateprojectpluginview.cpp:230
#, kde-format
msgid "Checkout Git Branch"
msgstr ""

#: kateprojectpluginview.cpp:236 kateprojectpluginview.cpp:810
#, kde-format
msgid "Lookup: %1"
msgstr "Vyhledat: %1"

#: kateprojectpluginview.cpp:237 kateprojectpluginview.cpp:811
#, kde-format
msgid "Goto: %1"
msgstr "Přejít na: %1"

#: kateprojectpluginview.cpp:311
#, kde-format
msgid "Projects Index"
msgstr "Index projektu"

#: kateprojectpluginview.cpp:855
#, kde-format
msgid "Choose a directory"
msgstr "Vyberte adresář"

#: kateprojecttreeviewcontextmenu.cpp:42
#, kde-format
msgid "Enter name:"
msgstr "Zadejte název:"

#: kateprojecttreeviewcontextmenu.cpp:43
#, kde-format
msgid "Add"
msgstr "Přidat"

#: kateprojecttreeviewcontextmenu.cpp:64
#, kde-format
msgid "Copy Location"
msgstr "Kopírovat umístění"

#: kateprojecttreeviewcontextmenu.cpp:69
#, kde-format
msgid "Add File"
msgstr "Přidat soubor"

#: kateprojecttreeviewcontextmenu.cpp:70
#, kde-format
msgid "Add Folder"
msgstr "Přidat složku"

#: kateprojecttreeviewcontextmenu.cpp:77
#, kde-format
msgid "&Rename"
msgstr "&Přejmenovat"

#: kateprojecttreeviewcontextmenu.cpp:84
#, kde-format
msgid "Properties"
msgstr "Vlastnosti"

#: kateprojecttreeviewcontextmenu.cpp:88
#, kde-format
msgid "Open With"
msgstr "Otevřít pomocí"

#: kateprojecttreeviewcontextmenu.cpp:96
#, kde-format
msgid "Open Internal Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:105
#, kde-format
msgid "Open External Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:110
#, kde-format
msgid "&Open Containing Folder"
msgstr "&Otevřít odpovídající složku"

#: kateprojecttreeviewcontextmenu.cpp:119
#, kde-format
msgid "Show Git History"
msgstr "Zobrazit historii Gitu"

#: kateprojecttreeviewcontextmenu.cpp:124
#, kde-format
msgid "Delete File"
msgstr "Smazat soubor"

#: kateprojecttreeviewcontextmenu.cpp:125
#, kde-format
msgid "Do you want to delete the file '%1'?"
msgstr ""

#: kateprojectviewtree.cpp:136
#, kde-format
msgid "Failed to create file: %1, Error: %2"
msgstr "Nelze vytvořit soubor: %1, Chyba: %2"

#: kateprojectviewtree.cpp:157
#, kde-format
msgid "Failed to create dir: %1"
msgstr "Nelze vytvořit adresář: %1"

#: stashdialog.cpp:36
#, kde-format
msgid "Stash message (optional). Enter to confirm, Esc to leave."
msgstr ""

#: stashdialog.cpp:43
#, kde-format
msgid "Type to filter, Enter to pop stash, Esc to leave."
msgstr ""

#: stashdialog.cpp:142
#, kde-format
msgid "Failed to stash changes %1"
msgstr ""

#: stashdialog.cpp:144
#, kde-format
msgid "Changes stashed successfully."
msgstr ""

#: stashdialog.cpp:163
#, kde-format
msgid "Failed to get stash list. Error: "
msgstr ""

#: stashdialog.cpp:179
#, kde-format
msgid "Failed to apply stash. Error: "
msgstr ""

#: stashdialog.cpp:181
#, kde-format
msgid "Failed to drop stash. Error: "
msgstr ""

#: stashdialog.cpp:183
#, kde-format
msgid "Failed to pop stash. Error: "
msgstr ""

#: stashdialog.cpp:187
#, kde-format
msgid "Stash applied successfully."
msgstr ""

#: stashdialog.cpp:189
#, kde-format
msgid "Stash dropped successfully."
msgstr ""

#: stashdialog.cpp:191
#, kde-format
msgid "Stash popped successfully."
msgstr ""

#: stashdialog.cpp:220
#, kde-format
msgid "Show stash failed. Error: "
msgstr ""

#. i18n: ectx: Menu (projects)
#: ui.rc:9
#, kde-format
msgid "&Projects"
msgstr "&Projekty"
