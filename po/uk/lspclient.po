# Translation of lspclient.po to Ukrainian
# Copyright (C) 2019-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: lspclient\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:36+0000\n"
"PO-Revision-Date: 2023-08-25 09:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:259
#, kde-format
msgid "Filter..."
msgstr "Фільтрувати…"

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:99 lspclientconfigpage.cpp:104
#: lspclientpluginview.cpp:452 lspclientpluginview.cpp:607 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "Клієнт LSP"

#: lspclientconfigpage.cpp:214
#, kde-format
msgid "No JSON data to validate."
msgstr "Немає доступних даних JSON."

#: lspclientconfigpage.cpp:223
#, kde-format
msgid "JSON data is valid."
msgstr "Дані JSON є коректними."

#: lspclientconfigpage.cpp:225
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Дані JSON є некоректними: немає об'єкта JSON"

#: lspclientconfigpage.cpp:228
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Дані JSON є некоректними: %1"

#: lspclientconfigpage.cpp:276
#, kde-format
msgid "Delete selected entries"
msgstr "Вилучити позначені записи"

#: lspclientconfigpage.cpp:281
#, kde-format
msgid "Delete all entries"
msgstr "Вилучити усі записи"

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr "Надіслано запит на запуск сервера LSP"

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""
"Хочете запустити сервер LSP?<br><br>Повний рядок команди є таким:<br><br><b>"
"%1</b><br><br>Вибір можна змінити за допомогою сторінки налаштувань додатка."

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""
"Користувачем остаточно заблоковано запуск «%1».\n"
"Скористайтеся сторінкою налаштувань додатка, щоб скасувати це блокування."

#: lspclientpluginview.cpp:419 lspclientpluginview.cpp:671
#, kde-format
msgid "LSP"
msgstr "LSP"

#: lspclientpluginview.cpp:481
#, kde-format
msgid "Go to Definition"
msgstr "Перейти до визначення"

#: lspclientpluginview.cpp:483
#, kde-format
msgid "Go to Declaration"
msgstr "Перейти до оголошення"

#: lspclientpluginview.cpp:485
#, kde-format
msgid "Go to Type Definition"
msgstr "Перейти до визначення типу"

#: lspclientpluginview.cpp:487
#, kde-format
msgid "Find References"
msgstr "Знайти посилання"

#: lspclientpluginview.cpp:490
#, kde-format
msgid "Find Implementations"
msgstr "Знайти реалізації"

#: lspclientpluginview.cpp:492
#, kde-format
msgid "Highlight"
msgstr "Підсвітити"

#: lspclientpluginview.cpp:494
#, kde-format
msgid "Symbol Info"
msgstr "Відомості щодо символів"

#: lspclientpluginview.cpp:496
#, kde-format
msgid "Search and Go to Symbol"
msgstr "Знайти символ і перейти до нього"

#: lspclientpluginview.cpp:501
#, kde-format
msgid "Format"
msgstr "Форматувати"

#: lspclientpluginview.cpp:504
#, kde-format
msgid "Rename"
msgstr "Перейменувати"

#: lspclientpluginview.cpp:507
#, kde-format
msgid "Expand Selection"
msgstr "Розгорнути позначене"

#: lspclientpluginview.cpp:510
#, kde-format
msgid "Shrink Selection"
msgstr "Стиснути позначене"

#: lspclientpluginview.cpp:513
#, kde-format
msgid "Switch Source Header"
msgstr "Перемкнути заголовок джерела"

#: lspclientpluginview.cpp:516
#, kde-format
msgid "Expand Macro"
msgstr "Розгорнути макрос"

#: lspclientpluginview.cpp:518
#, kde-format
msgid "Code Action"
msgstr "Дія з кодом"

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Show selected completion documentation"
msgstr "Показувати документацію щодо доповнення позначеного"

#: lspclientpluginview.cpp:536
#, kde-format
msgid "Enable signature help with auto completion"
msgstr "Увімкнути довідку щодо підписів із автодоповненням"

#: lspclientpluginview.cpp:539
#, kde-format
msgid "Include declaration in references"
msgstr "Включати оголошення до посилань"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:542 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr "Додавати дужки при доповненні функцій"

#: lspclientpluginview.cpp:545
#, kde-format
msgid "Show hover information"
msgstr "Показувати відомості при наведенні"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:548 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr "Форматувати при введенні"

#: lspclientpluginview.cpp:551
#, kde-format
msgid "Incremental document synchronization"
msgstr "Нарощувальна синхронізація документів"

#: lspclientpluginview.cpp:554
#, kde-format
msgid "Highlight goto location"
msgstr "Підсвічувати місця переходу"

#: lspclientpluginview.cpp:563
#, kde-format
msgid "Show Inlay Hints"
msgstr "Показати вкладені підказки"

#: lspclientpluginview.cpp:567
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr "Показувати діагностичні сповіщення"

#: lspclientpluginview.cpp:572
#, kde-format
msgid "Show Messages"
msgstr "Показувати повідомлення"

#: lspclientpluginview.cpp:577
#, kde-format
msgid "Server Memory Usage"
msgstr "Використання пам'яті сервера"

#: lspclientpluginview.cpp:581
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr "Закрити усі вкладки із динамічними посиланнями"

#: lspclientpluginview.cpp:583
#, kde-format
msgid "Restart LSP Server"
msgstr "Перезапустити сервер LSP"

#: lspclientpluginview.cpp:585
#, kde-format
msgid "Restart All LSP Servers"
msgstr "Перезапустити усі сервери LSP"

#: lspclientpluginview.cpp:596
#, kde-format
msgid "Go To"
msgstr "Перейти"

#: lspclientpluginview.cpp:623
#, kde-format
msgid "More options"
msgstr "Додаткові параметри"

#: lspclientpluginview.cpp:829 lspclientsymbolview.cpp:295
#, kde-format
msgid "Expand All"
msgstr "Розгорнути усі"

#: lspclientpluginview.cpp:830 lspclientsymbolview.cpp:296
#, kde-format
msgid "Collapse All"
msgstr "Згорнути усі"

#: lspclientpluginview.cpp:1033
#, kde-format
msgid "RangeHighLight"
msgstr "Підсвічування діапазону"

#: lspclientpluginview.cpp:1345
#, kde-format
msgid "Line: %1: "
msgstr "Рядок: %1: "

#: lspclientpluginview.cpp:1497 lspclientpluginview.cpp:1845
#: lspclientpluginview.cpp:1964
#, kde-format
msgid "No results"
msgstr "Немає результатів"

#: lspclientpluginview.cpp:1556
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Визначення: %1"

#: lspclientpluginview.cpp:1562
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Оголошення: %1"

#: lspclientpluginview.cpp:1568
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Визначення типу: %1"

#: lspclientpluginview.cpp:1574
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Посилання: %1"

#: lspclientpluginview.cpp:1586
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Реалізація: %1"

#: lspclientpluginview.cpp:1599
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Підсвічування: %1"

#: lspclientpluginview.cpp:1623 lspclientpluginview.cpp:1634
#: lspclientpluginview.cpp:1647
#, kde-format
msgid "No Actions"
msgstr "Ніяких дій"

#: lspclientpluginview.cpp:1638
#, kde-format
msgid "Loading..."
msgstr "Завантаження…"

#: lspclientpluginview.cpp:1730
#, kde-format
msgid "No edits"
msgstr "Немає редагувань"

#: lspclientpluginview.cpp:1804
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Перейменування"

#: lspclientpluginview.cpp:1805
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "Нова назва (попередження: може бути замінено не усі посилання)"

#: lspclientpluginview.cpp:1851
#, kde-format
msgid "Not enough results"
msgstr "Недостатньо результатів"

#: lspclientpluginview.cpp:1920
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr "Не знайдено відповідного заголовка або джерела"

#: lspclientpluginview.cpp:2116 lspclientpluginview.cpp:2159
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr "Сервер LSP"

#: lspclientpluginview.cpp:2182
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "Клієнт LSP"

#: lspclientpluginview.cpp:2454
#, kde-format
msgid "Question from LSP server"
msgstr "Запитання з сервера LSP"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "Restarting"
msgstr "Перезапускаємо"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "NOT Restarting"
msgstr "НЕ перезапускаємо"

#: lspclientservermanager.cpp:605
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr "Роботу сервера несподівано перервано… %1 [%2] [домашня сторінка: %3] "

#: lspclientservermanager.cpp:801
#, kde-format
msgid "Failed to find server binary: %1"
msgstr "Не вдалося знайти виконуваний файл сервера: %1"

#: lspclientservermanager.cpp:804 lspclientservermanager.cpp:846
#, kde-format
msgid "Please check your PATH for the binary"
msgstr ""
"Будь ласка, перевірте, чи правильно вказано PATH для виконуваного файла"

#: lspclientservermanager.cpp:805 lspclientservermanager.cpp:847
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""
"Див. також %1 для ознайомлення із правилами встановлення та подробицями"

#: lspclientservermanager.cpp:843
#, kde-format
msgid "Failed to start server: %1"
msgstr "Не вдалося запустити сервер: %1"

#: lspclientservermanager.cpp:851
#, kde-format
msgid "Started server %2: %1"
msgstr "Запущено сервер %2: %1"

#: lspclientservermanager.cpp:886
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr "Не вдалося обробити налаштування сервера «%1»: немає об'єкта JSON"

#: lspclientservermanager.cpp:889
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr "Не вдалося обробити налаштування сервера «%1»: %2"

#: lspclientservermanager.cpp:893
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr "Не вдалося прочитати налаштування сервера: %1"

#: lspclientsymbolview.cpp:241
#, kde-format
msgid "Symbol Outline"
msgstr "Схема символів"

#: lspclientsymbolview.cpp:286
#, kde-format
msgid "Tree Mode"
msgstr "Режим ієрархії"

#: lspclientsymbolview.cpp:288
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Автоматично розгортати ієрархію"

#: lspclientsymbolview.cpp:290
#, kde-format
msgid "Sort Alphabetically"
msgstr "Упорядкувати за абеткою"

#: lspclientsymbolview.cpp:292
#, kde-format
msgid "Show Details"
msgstr "Показати подробиці"

#: lspclientsymbolview.cpp:453
#, kde-format
msgid "Symbols"
msgstr "Символи"

#: lspclientsymbolview.cpp:584
#, kde-format
msgid "No LSP server for this document."
msgstr "Немає сервера LSP для цього документа."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr "Параметри клієнта"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, kde-format
msgid "Format on save"
msgstr "Форматувати при збереженні"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr "Увімкнути семантичне підсвічування"

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr "Увімкнути вкладені підказки"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Completions:"
msgstr "Доповнення:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr "Показувати вбудовану документацію вибраного варіанта доповнення"

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr "Показувати підпис функції при введенні виклику функції"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr "Додавати під час доповнення імпортування автоматично, якщо це потрібно"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr "Навігація:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr "Враховувати оголошення при пошуку посилань на символ"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr "Показувати відомості щодо символу, на який наведено вказівник"

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr "Підсвічувати рядок призначення при переході до нього"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, kde-format
msgid "Server:"
msgstr "Сервер:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, kde-format
msgid "Show program diagnostics"
msgstr "Показувати діагностику програми"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr "Показувати сповіщення від сервера LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr "Нарощувально синхронізувати документи з сервером LSP"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr "Ескіз документа:"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Упорядкувати символи за абеткою"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, kde-format
msgid "Display additional details for symbols"
msgstr "Показувати додаткові подробиці щодо символів"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr "Показувати ієрархію символів, а не простий список"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, kde-format
msgid "Automatically expand tree"
msgstr "Автоматично розгортати ієрархію"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr "Дозволені і заблоковані сервери"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr "Параметри сервера користувача"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr "Файл параметрів:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr "Типові параметри сервера"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "Додаткові параметри"

#~ msgid "Quick Fix"
#~ msgstr "Швидке виправлення"

#~ msgid "Show Diagnostics Highlights"
#~ msgstr "Показувати підсвічування діагностики"

#~ msgid "Show Diagnostics Marks"
#~ msgstr "Показувати позначки діагностики"

#~ msgid "Show Diagnostics on Hover"
#~ msgstr "Показувати діагностику при наведенні"

#~ msgid "Switch to Diagnostics Tab"
#~ msgstr "Перемкнутися на вкладку діагностики"

#~ msgctxt "@title:tab"
#~ msgid "Diagnostics"
#~ msgstr "Діагностика"

#~ msgid "Error"
#~ msgstr "Помилка"

#~ msgid "Warning"
#~ msgstr "Попередження"

#~ msgid "Information"
#~ msgstr "Інформація"

#~ msgctxt "@info"
#~ msgid ""
#~ "Error in regular expression: %1\n"
#~ "offset %2: %3"
#~ msgstr ""
#~ "Помилка у формальному виразі: %1\n"
#~ "відступ %2: %3"

#~ msgid "Copy to Clipboard"
#~ msgstr "Скопіювати до буфера"

#~ msgid "Remove Global Suppression"
#~ msgstr "Вилучити загальне придушення"

#~ msgid "Add Global Suppression"
#~ msgstr "Додати загальне придушення"

#~ msgid "Remove Local Suppression"
#~ msgstr "Вилучити локальне придушення"

#~ msgid "Add Local Suppression"
#~ msgstr "Додати локальне придушення"

#~ msgid "Disable Suppression"
#~ msgstr "Вимкнути придушення"

#~ msgid "Enable Suppression"
#~ msgstr "Увімкнути придушення"

#~ msgctxt "@info"
#~ msgid "%1 [suppressed: %2]"
#~ msgstr "%1 [придушено: %2]"

#~ msgid "Diagnostics:"
#~ msgstr "Діагностика:"

#~ msgid "Highlight lines with diagnostics"
#~ msgstr "Підсвічувати рядки з діагностикою"

#~ msgid "Show markers in the margins for lines with diagnostics"
#~ msgstr "Показувати позначки на полях для рядків з діагностикою"

#~ msgid "Show diagnostics on hover"
#~ msgstr "Показувати діагностику при наведенні"

#~ msgid "max diagnostics tooltip size"
#~ msgstr "Розмір панелі підказки максимальної діагностики"

#~ msgid "Type to filter through symbols in your project..."
#~ msgstr "Введіть критерій фільтрування символів у вашому проєкті…"

#~ msgid "LSP Client Symbol Outline"
#~ msgstr "Схема символів клієнта LSP"

#~ msgid "General Options"
#~ msgstr "Загальні параметри"

#~ msgid "Add highlights"
#~ msgstr "Додати підсвічування"

#~ msgid "Add markers"
#~ msgstr "Додати позначки"

#~ msgid "On hover"
#~ msgstr "При наведенні"

#~ msgid "Tree mode outline"
#~ msgstr "Схема режиму ієрархії"

#~ msgid "Automatically expand nodes in tree mode"
#~ msgstr "Автоматично розгортати вузли у режимі ієрархії"

#~ msgid "Switch to messages tab upon message level"
#~ msgstr "Перемкнутися на вкладку повідомлень через рівень повідомлень"

#~ msgctxt "@info"
#~ msgid "Never"
#~ msgstr "Ніколи"

#~ msgctxt "@info"
#~ msgid "Error"
#~ msgstr "Помилка"

#~ msgctxt "@info"
#~ msgid "Warning"
#~ msgstr "Попередження"

#~ msgctxt "@info"
#~ msgid "Information"
#~ msgstr "Відомості"

#~ msgctxt "@info"
#~ msgid "Log"
#~ msgstr "Журнал"

#~ msgid "Switch to messages tab"
#~ msgstr "Перемкнутися на вкладку повідомлень"

#~ msgctxt "@title:tab"
#~ msgid "Messages"
#~ msgstr "Повідомлення"

#~ msgctxt "@info"
#~ msgid "Unknown"
#~ msgstr "Немає відомостей"

#~ msgid "Switch to messages tab upon level"
#~ msgstr "Перемкнутися на вкладку повідомлень через рівень"

#~ msgid "Never"
#~ msgstr "Ніколи"

#~ msgid "Log"
#~ msgstr "Журнал"

#~ msgid "Hover"
#~ msgstr "Наведення"

#~ msgctxt "@info"
#~ msgid "<b>LSP Client:</b> %1"
#~ msgstr "<b>Клієнт LSP:</b> %1"

#~ msgid "Server Configuration"
#~ msgstr "Налаштування сервера"

#~| msgid "Format"
#~ msgid "Form"
#~ msgstr "Форма"

#~ msgid "Format on typing (newline)"
#~ msgstr "Форматувати при введенні (новий рядок)"

#~ msgid "No Hover Info"
#~ msgstr "Немає даних"

#~ msgctxt "@title:column"
#~ msgid "Position"
#~ msgstr "Позиція"

#~ msgid "No outline items"
#~ msgstr "Немає записів схеми"

#~ msgid "No server available"
#~ msgstr "Немає доступних серверів"
